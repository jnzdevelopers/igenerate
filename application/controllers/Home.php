<?php
//igen home page controller
defined('BASEPATH') or exit('No direct script access allowed');
class Home extends ClientsController {


    public function index(){

        $this->load->view('website/index');

    }

    public function about(){

        $this->load->view('website/about');
    }

    public function cost(){
        
        $this->load->view('website/cost');
    }

    public function campaigns(){
        
        $this->load->view('website/campaigns');
    }

    public function contact(){

        $this->load->view('website/contact');

    }
    public function send_mail(){
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['subject'] = $this->input->post('subject');
        $data['message'] = $this->input->post('message');
	
//	print_r($data); exit;	
        $email_body = '
                <!Doctype html>
                <html lang="en">
                    <head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                        <title>iGenerate :: Contact Form</title>
                    </head>
                    <body bgcolor="#fff">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 0 0 30px 0;">
            
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                                        <tr>
                                            <td align="center" bgcolor="#222" style="padding: 40px 0 30px 0; color: #FFF; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                                                 <h2>iGenerate </h2>
                                            </td>
            
            
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="color: #222; font-family: Arial, sans-serif; font-size: 24px;">
                                                            <b>Contact Form</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 20px 0 30px 0; color: #7B6C63; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                                                <table width="100%">
                                                            <tr>
                                                                <td><strong>Subject</strong></td>
                                                                <td>'.$data['subject'].'</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Name</strong></td>
                                                                <td>'.$data['name'].'</td>
                                                            </tr>
            
                                                            <tr>
                                                                <td><strong>Email</strong></td>
                                                                <td>'.$data['email'].'</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Message</strong></td>
                                                                <td>
                                                                    '.nl2br($data['message']).'
                                                                </td>
                                                            </tr>
            
                                                        </table>
                                                                                </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#222" style="padding: 30px 30px 30px 30px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
                                                          &copy; ' . date('Y') . '
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>
        
            ';
            
        $result = $this->send_email(['from' => $data['email'],
                                     'subject' => $data['subject'], 
                                     'message' => $email_body, 
                                     'resource_file' => '']);
	$data['status'] = $result;
	//print_r($data); exit;
        //$this->db->insert('emails', $data);
        redirect($_SERVER['HTTP_REFERER']);
    }



    public function send_email($params){
        // Send email
        $config['useragent'] = 'iGenerate';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->load->library('email', $config);
        $this->email->from($params['from'], 'WebForm');
        $this->email->to('info@igenerate.co.za');
        // if(isset($params['cc_email'])){
        //  $this->email->cc($params['cc_email']);
        // }
        $this->email->subject($params['subject']);
        $this->email->message($params['message']);
        if ($params['resource_file'] != '') {
            $this->email->attach($params['resourceed_file']);
        }
        $send = $this->email->send();
        if ($send) {
            // return $send;
            return 1;
        } else {
             $error = show_error($this->email->print_debugger());
            // return $error;
            return 0;
        }
    }



}



